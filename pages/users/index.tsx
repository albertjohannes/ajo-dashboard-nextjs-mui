import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Title from '../../components/Title';
import styles from '../../styles/Users.module.css';

interface UserProps {
    dataUsers: Array<any>;
}

export default function Users(props:UserProps) {
  const { dataUsers } = props;
  //   console.log(dataUsers);
  const router = useRouter();

  return (
    <Layout pageTitle="User Page">
      <Title>Main Users</Title>
      {/* <p>Main Users</p> */}
      <>
        {dataUsers.map((user) => (
          <div key={user.id} className={styles.card}>
            <p>{user.name}</p>
            <p>{user.email}</p>
            <button 
              type="submit" 
              onClick={() => router.push(`/users/${user.id}`)} 
              onKeyDown={() => router.push(`/users/${user.id}`)}
            >
              See details
            </button>
          </div>
        ))}
      </>
    </Layout>
  );
}

export async function getStaticProps() {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const dataUsers = await res.json();
  return {
    props: {
      dataUsers,
    },
  };
}
