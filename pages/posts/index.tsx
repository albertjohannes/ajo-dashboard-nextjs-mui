import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Title from '../../components/Title';
import styles from '../../styles/Post.module.css';

interface Post {
    id: number;
    title: string;
    body: string;
}

interface PostProps {
    dataBlog: Post[]
}

export default function Posts(props: PostProps) {
  const { dataBlog } = props;
  const router = useRouter();

  return (
    <Layout pageTitle="Post Page">
      <Title>Main Posts</Title>
      {/* <p>Main Posts</p> */}
      <button type="button" onClick={() => router.push('/posts/create/')}>
        Create new Post
      </button>
      {dataBlog.map((post) => (
        <div key={post.id} className={styles.card}>
          <h3>{`Title : (${post.id}) ${post.title}`}</h3>
          <p>{`Body : ${post.body}`}</p>
          <button type="submit" onClick={() => router.push(`/posts/${post.id}`)} onKeyDown={() => router.push(`/posts/${post.id}`)} aria-hidden="true">See comments</button>
        </div>
      ))}
    </Layout>
  );
}

export async function getServerSideProps() {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts/');
  const dataBlog = await res.json();
  return {
    props: {
      dataBlog,
    },
  };
}
