import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Title from '../../components/Title';
import styles from '../../styles/Post.module.css';

interface Comment {
  id: number;
  postId: number;
  name: string;
  email: string;
  body: string;
}

interface CommentProps {
  dataComment: Comment[];
}

export default function UserDetail(props: CommentProps) {
  const router = useRouter();
  const { dataComment } = props;
  // console.log(dataComment);
  return (
    <Layout pageTitle="Comment Detail">
      <Title>List of Comments</Title>
      {/* <p>List of Comments</p> */}
      <button type="button" onClick={() => router.back()}>
        Back
      </button>
      <div className={styles.card}>
        {dataComment.map((comment) => (
          <div key={comment.id} className={styles.card}>
            <p>
              Post ID :
              {' '}
              {' '}
              {comment.postId}
              {' '}
              {' '}
              | Comment ID :
              {' '}
              {' '}
              {comment.id}
            </p>
            <p>
              Name :
              {' '}
              {comment.name}
            </p>
            <p>
              {' '}
              Email :
              {' '}
              {comment.email}

            </p>
            <p>
              {' '}
              Body :
              {' '}
              {comment.body}

            </p>
          </div>
        ))}
      </div>
    </Layout>
  );
}

interface Post {
  id: number;
  title: string;
  body: string;
}

export async function getStaticPaths() {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts');
  const dataComment = await res.json();

  const paths = dataComment.map((post: Post) => ({
    params: {
      id: `${post.id}`,
    },
  }));
  return {
    paths,
    fallback: false,
  };
}

interface GetStaticProps {
  params: {
    id: string;
  }
}

export async function getStaticProps(context: GetStaticProps) {
  const { id } = context.params;
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}/comments`);
  const dataComment = await res.json();

  return {
    props: {
      dataComment,
    },
  };
}
