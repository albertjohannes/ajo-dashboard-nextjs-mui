import { useRouter } from 'next/router';
import { useState } from 'react';
import Layout from '../../components/Layout';
import styles from '../../styles/Post.module.css';
import Alert from '../../components/Alert';
import Title from '../../components/Title';

export default function AddPost() {
  const router = useRouter();
  const [alert, setAlert] = useState(false);
  const [resId, setResId] = useState(0);

  const submitPost = async (event:any) => {
    event.preventDefault();

    const res = await fetch('https://jsonplaceholder.typicode.com/posts', {
      body: JSON.stringify({
        name: event.target.name.value,
        body: event.target.body.value,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });

    const result = await res.json();
    if (res.status === 201) {
      event.target.reset();
      setAlert(true);
      setResId(result.id);
      console.log(res.status, result);
    }
  };

  return (
    <Layout pageTitle="Post Page">
      <Title>Create New Posts</Title>
      <p>Create New Posts</p>
      <button type="button" onClick={() => router.back()}>
        Back
      </button>
      <div>
        {alert
          ? (
            <Alert type="warning">
              {`Post Submitted under ID : ${resId} is Success`}
            </Alert>
          )
          : <></>}
      </div>
      <div className="form">
        <form onSubmit={submitPost} className={styles['card-form']}>
          <div>
            <label htmlFor="name">
              Name
            </label>
            <input id="name" name="name" type="text" autoComplete="name" required />
          </div>
          <div>
            <label htmlFor="name">
              Body          
            </label>
            <input id="body" name="body" type="text" autoComplete="body" required />
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>
    </Layout>
  );
}
