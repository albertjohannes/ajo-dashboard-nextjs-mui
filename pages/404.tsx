import { useEffect } from 'react';
import { useRouter } from 'next/dist/client/router';
import { Paper, Typography } from '@mui/material'

export default function Custom404() {
  const router = useRouter();

  useEffect(() => {
    const timer = setTimeout(() =>  router.push('/'), 3000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Paper
        sx={{
          backgroundColor: (t) => t.palette.background.default,
          margin: 0,
          height: `calc(100vh - 64px)`,
        }}
      >
    <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: `100%`,
          }}
        >
          <Typography variant="h4">404</Typography>
          <Typography variant="subtitle1">
            Sorry, page not found
          </Typography>
        </div>
    </Paper>
  );
}
