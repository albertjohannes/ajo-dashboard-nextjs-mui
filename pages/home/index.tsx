import Image from 'next/image';
import styles from "../../styles/Home.module.css";
import Layout from '../../components/LayoutHome';
  
export default function Home() {
    return (
      <Layout pageTitle="Home">
        <div className={styles['flex-container']}>
          <div className={styles['flex-section']}>
            <Image src="/profile.png" width="150" height="150" alt="profile" />
            <Image src="/profile.png" width={200} height={200} alt="profile" />
          </div>
          <div className={styles['flex-section']}>
            <h1 className="title">AJO-Dashboard NextJS with MUI</h1>
            <h1 className={styles['title-homepage']}>Homepage</h1>
          </div>
          <div className={styles['flex-section']}>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
          </div>
        </div>
      </Layout>
    );
}