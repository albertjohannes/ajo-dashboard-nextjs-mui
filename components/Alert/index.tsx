interface AlertProps {
    type: string;
    children: string;
  }

export default function Alert(props:AlertProps) {
  const { type, children } = props;
  return (
    <div className={type === 'warning' ? 'alert warning' : 'alert'}>
      {children}
      <style jsx>
        {`
        .alert {
          display: inline-block;
          padding: 20px;
          border-radius: 8px;
          background: #eee;
        }
        .alert.warning {
          background: #fff3cd;
        }
      `}
      </style>
    </div>
  );
}
