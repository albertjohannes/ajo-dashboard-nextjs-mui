import * as React from 'react';
import Link from 'next/link';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import AssignmentIcon from '@mui/icons-material/Assignment';
import styles from './HeaderDrawer.module.css';

export const mainListItems = (
  <React.Fragment>
    <Link href="/home" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Home" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);

export const secondaryListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      JSONPlaceholder
    </ListSubheader>
    <Link href="/posts" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Posts" />
      </ListItemButton>
    </Link>
    <Link href="/users" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Users" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);

export const thirdListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      MUI Example
    </ListSubheader>
    <Link href="/signin/v1" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <LayersIcon />
        </ListItemIcon>
        <ListItemText primary="Sign In v1" />
      </ListItemButton>
    </Link>
    <Link href="/signin/v2" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <LayersIcon />
        </ListItemIcon>
        <ListItemText primary="Sign In v2" />
      </ListItemButton>
    </Link>
    <Link href="/signup" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <BarChartIcon />
        </ListItemIcon>
        <ListItemText primary="Sign Up" />
      </ListItemButton>
    </Link>
    <Link href="/checkout" className={styles.textlink}>
      <ListItemButton>
        <ListItemIcon>
          <ShoppingCartIcon />
        </ListItemIcon>
        <ListItemText primary="Checkout" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);
